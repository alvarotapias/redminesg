README
------

@Author: Álvaro Tapias 

Programa que consulta las peticiones del proyecto Redmine de secretaría general y realiza una serie de comprobaciones sobre la corrección de los datos.
Se implementan comprobaciones entre campos diferentes de las peticiones que no pueden ser realizados directamente en redmine. 

CLONE
git clone https://alvarotapias@bitbucket.org/alvarotapias/redminesg.git
Previamente hay que configurar proxy http y https si lo tenemos:
En windows:
	 SET HTTP_PROXY=USER:PASS@proxy.catastro.minhac.es:80
	 SET HTTPS_PROXY=USER:PASS@proxy.catastro.minhac.es:80

ENTORNO DE DESARROLLO: NETBEANS
JDK 8.121

COMPILACIÓN Y DESPLIEGUE: MAVEN
Instalación en ubuntu: 
	sudo apt-get update 
	sudo apt-get install maven

Instalación en windows: 
	descomprimir carpeta

Creación del pom.xml:
	mvn -B archetype:generate -DarchetypeGroupId=org.apache.maven.archetypes -DgroupId=es.gob.catastro -DartifactId=RedmineSG
	

EMPAQUETADO DE LA APLICACIÓN
  mvn install -P[profile]

Siendo [profile] el entorno de ejecución del programa. [profile] puede tener tres valores (está especificado en el fichero POM, dentro del apartado profiles, siendo local el profile por defecto):
    - local: entorno local del programador (por defecto)
    - test: entorno de pre (redmine-pre).
    - pro: entorno de pro (redmine).

    
Ejemplo: mvn install -Ppro
Nota: el profile es casesensitive

EJECUCIÓN DE LA APLICACIÓN
Por defecto, maven deja el .jar ejecutable en el directorio target.

Para ejecutarlo es necesario poner:
java -jar [fichero.jar]

LOG FILE
Configurado con LOG4J para guardarse en el directorio raíz.

CONFIGURACIÓN
Se requieren los siguientes ficheros de configuración:
- log4j.properties
- config.properties y ficheros específicos para pro y test
- Workflow.json (contiene las transiciones de estado)

EMAIL 
- Para el envío de correos en el entorno local, donde no es accesible el servidor smtp por el puerto 2525, se utiliza el servidor TestMailServer Tool (http://www.toolheap.com/test-mail-server-tool/)

- En los entornos de pre y pro se utiliza el servidor corporativo

CRONTAB EN REDMINE-PRE01 y REDMINE-PRO01
------------------------
URL CONSULTA: https://askubuntu.com/questions/216692/where-is-the-user-crontab-stored*control de documentación en nuxeo*/

* Editar: crontab -e
* Volcar crontab a mi directorio: crontab -l > mycrontab
* Si se modifica el fichero local se puede instalar luego: crontab mycrontab
* Para eliminarlo: crontab -r 


Para editar:
  1. /bin/nano        <---- easiest
  2. /usr/bin/emacs24
  3. /usr/bin/vim.nox
  4. /usr/bin/vim.tiny

Para compilar y poner en redmine-pre01 el ejecutable:
mvn clean install -Ptest
cd target
scp redminesg-1.0-Test-jar-with-dependencies.jar 52883895H@redmine-pre01:/home/52883895H

Para compilar y poner en redmine-pro01 el ejecutable:
mvn clean install -Ppro
cd target
scp redminesg-1.0-Pro-jar-with-dependencies.jar 52883895H@redmine-pro01:/home/52883895H

IMPORTANTE
----------
El programa envía comunicaciones vía mail. Antes de lanzar el proyecto hay que configurar la dirección de envío a través de los properties. 
En caso de querer incluir varias direcciones es posible utilizando la coma como separador.