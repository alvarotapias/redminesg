package es.gob.catastro.sg;

import org.apache.log4j.Logger;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.util.Properties;
import com.google.gson.Gson;
import java.util.*;
import javax.mail.*;
import javax.mail.internet.*;
import javax.activation.*;
import java.util.Locale;
import java.text.SimpleDateFormat;
import java.io.UnsupportedEncodingException;
import java.nio.charset.Charset;
import java.text.SimpleDateFormat; 
import java.util.Calendar;
import java.text.ParseException;
import java.lang.IllegalArgumentException;

/* clases del proyecto*/
import es.gob.catastro.sg.redmine.RedmineServer;
import es.gob.catastro.sg.expediente.*;
import es.gob.catastro.sg.warning.WarningData;

/**
 * Clase con utilidades de uso común 
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */
public class Helper {
	//Log
	static final Logger logger = Logger.getLogger(Helper.class);

	/**
	 * Lee el fichero de configuración de propiedaedes
	 * Existirán distintos ficheros en función del entorno de compilación
	 * @see README
	 * @param configFile Nombre del fichero de configuración
	 * @return Objeto con propiedades
	 * @throws SGException Error en la lectura del fichero de configuración
	 */
	public static Properties readProperties(String configFile) throws SGException {
		Properties props = null;

		try {
			props = new Properties();

			/* Lectura del fichero */		
			props.load(new InputStreamReader(
					Helper.class.getClassLoader().getResourceAsStream(configFile), 
					"ISO-8859-1"));

			logger.debug("<readProperties> Property file: " + props);
		} catch (IOException e) {
			logger.error("<readProperties> " + e);
			throw new SGException(e.getMessage());
		} catch (Exception e) {
			logger.error("<readProperties> " + e);
			throw new SGException(e.getMessage());
		}
		return props;
	}

	/**
	 * Lectura del workflow del fichero workflow.json 
	 * @return Objeto con flujos de trabajo leídos
	 * @throws SGException Error en la lectura del fichero
	 */
	public static ExpedientesWorkflow readWorkflow() 
	throws SGException {
		
		ExpedientesWorkflow eWorkflow = null;

		try {
			/*Lectura del fichero*/
			/*Se lee como un recurso y no como un fichero para que en linux se 
			  encuentre correctamente. En windows se encuentra de las dos formas*/
			Gson gson = new Gson();		
			BufferedReader br = 
				new BufferedReader(
					new InputStreamReader(
						Helper.class.getClassLoader().getResourceAsStream("statesWorkflow.json")));

			/*Mapeo a objeto*/
			eWorkflow = gson.fromJson(br, ExpedientesWorkflow.class);			

		} catch (Exception e) {
			logger.error("<readWorkflow> No se encuentra fichero \"workflow.json\"");
			throw new SGException(e.getMessage());
		}

		return eWorkflow;
	}


	/**
	 * Envio de correo con avisos
	 * @param warns Mensajes de warning a enviar
	 * @param configProperties Propiedades de configuración del correo
	 * @param server Servidor redmine
	 * @thows Error en el envío del correo
	*/
	public static void sendInfoMail(WarningData warns, 
	Properties configProperties, RedmineServer server) throws SGException {
		
	  	// Se lee la configuración para cada tipo de correo
		String to = warns.getMailTo();
		String subject = warns.getMailSubject();

		if (to == null || subject == null) 
			throw new SGException("No hay destinatario y asunto para el envío del mensaje");		

		// Se lee la configuración general del correo
		String from = configProperties.getProperty("mail.from");    
		String host = configProperties.getProperty("mail.smtp.host");
		String port = configProperties.getProperty("mail.smtp.port");
      	String debugMode = configProperties.getProperty("mail.debug.mode");
      	
      	// Se establecen propiedades con lo leido previamente 
      	// para el objeto sesion, sobre el que se envia
		Properties mailProperties = System.getProperties();
		mailProperties.setProperty("mail.smtp.host", host);     
	 	mailProperties.setProperty("mail.smtp.port", port);

	 	// activar para debug
		//mailProperties.setProperty("mail.debug", debugMode);
	 	
		Session session = Session.getDefaultInstance(mailProperties);		

		try {
			// se construye el mensaje
			MimeMessage message = new MimeMessage(session);
			message.setFrom(new InternetAddress(from));         
			
			// destinatario
			/*message.addRecipient(Message.RecipientType.TO, 
				new InternetAddress(to));         */
			if (to.indexOf(',') > 0)
				message.setRecipients(Message.RecipientType.TO, 
					InternetAddress.parse(to));         
			else 
				message.setRecipient(Message.RecipientType.TO, 
					new InternetAddress(to));         			

			// asunto
			/*Nota: Mucho problema con el juego de caracteres del mensaje.
			        Las tildes en mayúsculas llegan mal*/
			message.setSubject(
					subject 
					+ "[" + Helper.formatDateToString(new Date()) + "]"
					+ "[v" + configProperties.getProperty("version") + "]"
					, "utf-8");         	
			// body			
			String html = warns.toHtml(server);
			logger.debug("<sendInfoMail>" +html);
			message.setContent(html, "text/html; charset=utf-8");			

         	// Envío 
			Transport.send(message);    
			
		} catch (MessagingException e) {			
			logger.error("<sendInfoMail>" + e);
			throw new SGException(e.getMessage());
		}
	}

	/**
	 * Formatea una fecha en castellano
	 * @param date Fecha a formatear
	 * @return Fecha en formato "dd 'de' MMMM 'de' yyyy 'a las' HH:mm"
	*/
	public static String formatDateToString(Date date) throws IllegalArgumentException{
		SimpleDateFormat dateFormater = new SimpleDateFormat(
				"dd/MM/yyyy 'a las' HH:mm", new Locale("es_ES"));

		return dateFormater.format(date);
	}

	/**
	 * Formatea una fecha en formato redmine 
	 * @param String fecha en formato yyyy-MM-dd a formatear
	 * @return Fecha en formato "dd 'de' MMMM 'de' yyyy 'a las' HH:mm"
	 * @throws ParseException
	*/
	public static Date formatDateFromString(String value) throws ParseException {
		SimpleDateFormat dateFormater = new SimpleDateFormat("yyyy-MM-dd");
		return dateFormater.parse(value);			
	}

	/**
	  * Obtiene fecha de ayer en formato días
	*/
	public static Date getYesterday() {
    	Calendar cal = Calendar.getInstance();
		/*le quitamos horas, minutos y segundos para igualarlo con redmine*/
    	cal.set(Calendar.HOUR_OF_DAY, 0);
		cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MILLISECOND, 0);
		/*le restamos un día*/    	    	
    	cal.add(Calendar.DATE, -1);
    	return cal.getTime();
	}

	/**
	  * Obtiene fecha de hoy en formato días
	*/
	public static Date getToday() {
    	Calendar cal = Calendar.getInstance(); 
    	/*le quitamos horas, minutos y segundos para igualarlo con redmine*/
    	cal.set(Calendar.MILLISECOND, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	return cal.getTime();
	}

	/**
	  * Retorna la máxima fecha de facturación del año en el 
	  * que comienza el contrato (10 de noviembre del año)
	*/
	public static Date getMaxFechaFacturacion(Date fechaInicio) {
		Calendar cal = Calendar.getInstance(); 
		cal.setTime(fechaInicio);
    	/*le quitamos horas, minutos y segundos para igualarlo con redmine*/
    	cal.set(Calendar.MILLISECOND, 0);
    	cal.set(Calendar.SECOND, 0);
    	cal.set(Calendar.MINUTE, 0);
    	cal.set(Calendar.HOUR_OF_DAY, 0);
    	/*la fijamos en el 10 de noviembre del año */
    	cal.set(Calendar.DATE, 10);
    	cal.set(Calendar.MONTH, 10); //Ojo: 0 es enero
    	return cal.getTime();
	}

	/**
	  * Obtiene el año en dos cifras
	*/
	public static String getYearOfDate(Date d) {
    	SimpleDateFormat dateFormater = new SimpleDateFormat("yy");
		return dateFormater.format(d);    	
	}	

	/**
	  * Obtiene el mes en dos cifras
	*/
	public static int getMonthOfDate(Date d) {
    	SimpleDateFormat dateFormater = new SimpleDateFormat("MM");
		return Integer.parseInt(dateFormater.format(d));    	
	}	

	/**
	  * Obtiene el día en dos cifras
	*/
	public static int getDayOfDate(Date d) {
    	SimpleDateFormat dateFormater = new SimpleDateFormat("dd");
		return Integer.parseInt(dateFormater.format(d));	
	}

	/**
	  * Suma un año a la fecha
	*/
	public static Date addAYear(Date d) {
    	Calendar cal = Calendar.getInstance(); 
    	cal.setTime(d);
    	cal.add(Calendar.YEAR, 1);    	
    	return cal.getTime();
	}

	/**
	 * Formatea un texto a castellano (con acentos)
	 * @param s String a formatear
	 * @return Texto formateado
	*/
	public static String formatTextToUTF8(String s) {
		String formatString = s;
		try {
			//logger.debug("formatText IN:" + s);			
			/*formatear*/
			/*la visualización funciona para las minúsculas pero no para
			las mayúsculas en windows*/
						
			// fallo en todas las mayúsculas acentuadas en windows
			//byte[] ptext = s.getBytes("iso-8859-1");						
			//formatString = new String(ptext, "utf-8");

			// fallo solo en I,O y A mayúsculas acentuadas
			//@see http://docs.oracle.com/javase/7/docs/technotes/guides/intl/encoding.doc.html
			byte[] ptext = s.getBytes("windows-1252");									
			formatString = new String(ptext, "utf-8");
			
		} catch (UnsupportedEncodingException e) {
			logger.debug("<formatTextToUTF8>" + e);
		}

		return formatString;
	}		

}
