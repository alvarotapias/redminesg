package es.gob.catastro.sg;

import org.apache.log4j.Logger;
import java.util.Properties;

/* Clases del proyecto */
import es.gob.catastro.sg.redmine.*;
import es.gob.catastro.sg.expediente.*;
import es.gob.catastro.sg.warning.*;


/**
 * Proceso principal: Obtiene los elementos de redmine (proyecto SG)
 * y verifica los datos de las peticiones en base a ciertos criterios
 * 
 * @author Álvaro Tapias
 * @version 1.0 
 */
public class MainProcess 
{
	// fichero de propiedades
	public static final String configFile = "config.properties";

	// log
	 static final Logger logger = Logger.getLogger(MainProcess.class);

	/*
     * Función principal 
     *
     * @author Álvaro Tapias     
     * @throws SGException General error
	*/
    public static void main(String[] args) throws SGException {
    	
    	logger.info("<MAIN> PROCESS::Begin::");
    	
    	try {							
			/*Lectura del fichero de propiedaes*/
			Properties props = Helper.readProperties(configFile);			

			/*Lectura del fichero de workflow */
			ExpedientesWorkflow workflow = Helper.readWorkflow();
			logger.debug("<MAIN> JSON: " + workflow);	

			/*Creación del servidor*/
			RedmineServer server = new RedmineServer(props);
			logger.debug("<MAIN> Server:" + server);
		
			/*Objeto con mensajes resultantes del procesamiento de expedientes*/
			WarningData revisionExpMensj = 
				new WarningData(props.getProperty("revision.mail.to"), 
					props.getProperty("revision.mail.subject"));		

			/*Objeto con mensajes para la Secretaria General*/
			WarningData avisosMensj = 
				new WarningData(props.getProperty("avisos.mail.to"), 
					props.getProperty("avisos.mail.subject"));		

			
			/*Se obtienen los elementos del servidor y se procesan*/
			RedmineRequestParameters param = 
				new RedmineRequestParameters(0, 100, null, "sort=updated_on:desc");		
			boolean moreRequests = true;
			RedmineResponse response = null;
			while (moreRequests) {
				/*Obtención de elementos*/
				response = server.getIssues(param);				

				if (response != null && response.getIssuesCount() > 0) {
					
					/* Procesamiento de expedientes*/
					ExpedientesProcessor.processAllIssues(response, workflow, revisionExpMensj, avisosMensj, server);					

					/* Se incrementa el offset de la petición, por si hay varias páginas */
					if (response.getIssuesCount() == param.getLimit()) 
						param.setOffset(param.getOffset() + param.getLimit());
					else 
						moreRequests = false;
				} else 
					moreRequests = false;
			}

			/*Impresión del resultado por pantalla*/			
			logger.info("<MAIN> DETALLE DEL PROCESO:");
			logger.info("<MAIN> # Elementos actualizados = " 
							+ ExpedientesProcessor.totalIssuesUpdated);
			logger.info("<MAIN> # Mensajes del procesamiento =\n" + revisionExpMensj.toString());
			logger.info("<MAIN> # Mensajes de aviso =\n" + avisosMensj.toString());

			//Correo de revisión
			Helper.sendInfoMail(revisionExpMensj, props, server);

			//Correo de avisos
			if (avisosMensj.getSize() > 0)
				Helper.sendInfoMail(avisosMensj, props, server);

		} catch (RedmineServerException e) {
			logger.error("<MAIN>" + e);						
			throw new SGException(e.getMessage());
		} catch (SGException e) {
			logger.error("<MAIN>" + e);						
			throw new SGException(e.getMessage());
		}			
		
		logger.info("<MAIN> PROCESS::End::");
    }
}
