package es.gob.catastro.sg;

/**
 * Excepción genérica para el proyecto
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */
public class SGException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public SGException(String msg) {
	    super(msg);
	}
}
