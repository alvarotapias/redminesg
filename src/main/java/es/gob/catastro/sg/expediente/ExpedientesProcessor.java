package es.gob.catastro.sg.expediente;

import org.apache.log4j.Logger;
import java.util.List;
import java.util.ArrayList;
import java.util.Date;
import java.util.Locale;
import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat; 
import java.text.ParseException;
import java.text.DecimalFormat;
import java.text.NumberFormat;

import es.gob.catastro.sg.warning.WarningData;
import es.gob.catastro.sg.redmine.*;
import es.gob.catastro.sg.Helper;


/**
 * Clase para el procesamiento de las peticiones recuparadas de redmine
 *
 * (Contiene todas las especificidades por tratar elementos
 *  de contratos sobre redmine)
 *  
 * @author Álvaro Tapias
 * @version 1.1 (se añade control de los nº de expedientes) 
 */
public class ExpedientesProcessor {

	static final Logger logger = 
		Logger.getLogger(ExpedientesProcessor.class);

	// importe límite para ir al S.E
	static final double IMPORTE_SEC_ESTADO = 600000;

	public static int totalIssuesUpdated = 0;

	/**
	 * Procesa todos los elementos recuperados del servidor
	 *
	 * @param response: Respuesta del servidor con toda la información
	 * @param workflow: Workflow de trabajo sobre los estados de los expedientes
	 * @param processExpMesj: Mensajes de aviso tras el procesamiento
	 * @param avisosMesj: Avisos 
	 * @param server: Servidor de Redmine 
	*/
	public static void processAllIssues(RedmineResponse response, 
		ExpedientesWorkflow workflow, WarningData processExpMesj, 
		WarningData avisosMesj, RedmineServer server) {
		
		
		for(RedmineIssue issue: response.getIssues()) {
			logger.debug("<processAllIssues> Procesando elemento: " + issue.getSubject());

			//chequeo de errores
			checkErrorOnIssue(issue, processExpMesj);
			
			//actualización de datos de la petición
			updateIssue(issue, response, workflow, processExpMesj, server);
			
			//generación de avisos para S.G
			//Nota: se realiza después de la actualización porque toma datos 
			//del elemento actualizado
			generateWarnings(issue, avisosMesj);			
		}
	}

	/**
	 * Procesa una petición para comprobar si tiene errores 
	 *
	 * @param issue: Elemento a procesar
	 * @param processExpMesj: Mensajes de aviso tras el procesamiento
	*/
	public static void checkErrorOnIssue(RedmineIssue issue, WarningData mesj) {				
		
		if (issue.isExpediente()) {

			/*comprobación de existencia de identificadores externos*/
			checkNumExpedientesProcessor(issue, mesj);	

			/*solo para expedientes (salvo que check indique que no)*/
			if (issue.isRevisaImportes()) {
				//comprobación de importes por año
				checkImportesProcessor(issue, mesj);		

				//la comprobación de importes y fechas se hará mientras 
				//la tramitación esté viva
				if (issue.isEnTramitacion()) {
					/*comprobación de importes por fecha de inicio del expediente*/
					checkFechaInicioImporteProcessor(issue, mesj);				
					/*comprobación de importes por fecha de fin del expediente*/
					checkFechaFinImporteProcessor(issue, mesj);				
				}
			}

		}	
	}

	/**
	 * Procesa una petición para actualizar su estado y su importe
	 *
	 * @param issue: Elemento a procesar
	 * @param response: Respuesta del servidor (contiene el resto de peticiones)
	 * @param workflow: Workflow de trabajo sobre los estados de los expedientes
	 * @param mesj: Mensajes de aviso tras el procesamiento
	 * @param server: Servidor de Redmine 
	*/
	public static void updateIssue(RedmineIssue issue, 
		RedmineResponse response, ExpedientesWorkflow workflow, 
		WarningData mesj, RedmineServer server) {

		/*calculo de la facturación - Solo para expedientes*/
		if (issue.isExpediente())
			updatePagosProcessor(issue, response.getIssues(), mesj);		


		/*calculo de estados - Para pagos y expedientes*/
		updateEstadosProcessor(issue, workflow, mesj, server);

		/*actualización del elemento*/
		if (issue.isUpdated()) {
			try {					
				//Se añade nota a la modificación explicando que el cambio es automático			
				String note = 
					"Cambio realizado automáticamente desde programa "
					+ "(" + Helper.formatDateToString(new Date()) + ")";
				
				issue.addNote(
					//Ojo: problemas con el juego de caracteres de la nota wn windows, hay que transformar
					//new String(note.getBytes("UTF-8"), "ISO-8859-1"));
					note);

				/*actualización en el servidor*/
				server.updateIssue(issue);
				/*variable global: número de expedientes actualizados*/
				totalIssuesUpdated++;				
				
				logger.debug("<updateIssue> Actualización OK de " + issue.getSubject());	
			} catch (RedmineServerException e) {
				logger.error("<updateIssue>" + e);
				String warningStr = "ERROR: No se puede actualizar la petición "
					+ issue.getId() + " con los cambios efectuados ("+ e + " )";

				//Se añade error en la actualización a los mensajes a mostrar				
				mesj.add(issue, warningStr);
			} 
			//Este catch solo tiene sentido en windows para el parseo del charset
			//de la nota
			/*catch (UnsupportedEncodingException e) {
				String warningStr = "No se puede actualizar la petición "
					+ issue.getId() + " por error al formatear las notas";
				logger.error(warningStr + " " + e);

				//Se añade error en la actualización a los mensajes a mostrar
				mesj.add(issue, warningStr);
			}*/	
		}
	}

	/**
	 * Genera avisos importantes fuera de errores
	 *
	 * @param issue: Elemento a procesar
	 * @param avisosMesj: Mensajes de aviso tras el procesamiento
	*/
	public static void generateWarnings(RedmineIssue issue, WarningData avisosMesj) {		
		
		if (issue.isExpediente()) {

			//Comprobamos si hay envios al S.E
			avisosEnviosSEProcessor(issue, avisosMesj);

			//Comprobamos si ha terminado la tramitación
			avisosFinTramitacion(issue, avisosMesj);

			//Finalización expedientes
			avisosFechaFinProcessor(issue, avisosMesj);
		}	
	}

	/**
	 * Comprueba si hay envíos para el S.E y genera aviso
	 * 
	 * Comprobaciones:
	 *	- Existen documentos enviados al SE
	 * 
	 * @param issue Elemento a procesar
	 * @param mesj Mensajes de error	 
	*/
	public static void avisosEnviosSEProcessor(RedmineIssue issue, 
		WarningData mesj) {
		
		// cálculo del importe total del contrato
		double importeTotal = 0;
		try {
			String importeTotalStr = issue.getRedmineCustomIssueFieldValue("$Importe total");
			if (importeTotalStr != null && !importeTotalStr.isEmpty()) 
				importeTotal = Float.parseFloat(importeTotalStr);			
		} catch (Exception e) {
			//Si no hay importe declarado se pone a cero para no comprobar
			//será el checkeador de errores del expediente el que avise de este 
			//problema
			importeTotal = 0;
		}		

		// fechas
		Date today = Helper.getToday();	
		Date yesterday = Helper.getYesterday();	
		
		//procesamiento de campos
		List<RedmineIssueCustomField> custom_fields = issue.getRedmineIssueCustomFields();
		for (int i = 0; i < custom_fields.size(); i++) {
			String warningStr = null;
			try {				
				String fieldName = custom_fields.get(i).getName();

				//Cromprobación: campo envío al S.E en encomiendas
				//Nota:se hace contains porque en windows fallan las tildes
				if (fieldName.contains("a S.E. para firma")){				
					String strSEvalue = custom_fields.get(i).getValue();					
					if (strSEvalue != null && !strSEvalue.isEmpty()) {				
						//formateador de fechas en el formato redmine 						
						Date dateSEValue = Helper.formatDateFromString(strSEvalue);							

						//si la fecha de envío es hoy se pone aviso
						if (dateSEValue.equals(today)) {
							warningStr = "Se ha enviado a la firma del Secretario de Estado hoy";
							mesj.add(issue, warningStr);
						} else {
							// se obtiene fecha de actualización
							Date updateDate = 
								Helper.formatDateFromString(issue.getUpdatedOn());

							//si la fecha de envío es de ayer y se ha actualizado el elemento hoy
							if (dateSEValue.equals(yesterday) && updateDate != null 
									&& updateDate.equals(today)) {
								warningStr = "Se envió ayer a la firma del Secretario de Estado";	
								mesj.add(issue, warningStr);
							}
						}	
					}
				}
				
				//Cromprobación: campo envío propuesta aprobación del gasto 
				//Nota:se hace contains porque en windows fallan las tildes
				if (importeTotal >= IMPORTE_SEC_ESTADO
						&& (fieldName.contains("propuesta gasto a ID") 
							|| fieldName.contains("compromiso de gasto a ID"))){

					String valueStr = custom_fields.get(i).getValue();					
					if (valueStr != null && !valueStr.isEmpty()) {
						//formateador de fechas en el formato redmine 						
						Date valueDate = Helper.formatDateFromString(valueStr);							

						if (valueDate.equals(today)) {							
							warningStr = "Se ha enviado documento de gasto (autorización o compromiso) al Secretario de Estado para su firma";
							mesj.add(issue, warningStr);
						} else {
							// se obtiene fecha de actualización
							Date updateDate = 
								Helper.formatDateFromString(issue.getUpdatedOn());

							//si la fecha de envío es de ayer y se ha actualizado el elemento hoy
							if (valueDate.equals(yesterday) && updateDate != null 
									&& updateDate.equals(today)) {
								warningStr = "Se ha enviado documento de gasto (autorización o compromiso) al Secretario de Estado para su firma";	
								mesj.add(issue, warningStr);
							}
						}	
					}

				}

			} catch (Exception e) { 
				logger.error("<avisosEnviosSEProcessor>" + e);
			}		
		}		
	}

	/**
	 * Comprueba si ha finalizado la tramitación
	 * 
	 * Comprobaciones:
	 *	- Se ha cambiado el estado a "Exp. Fin tramites (Adju.)"
	 * 
	 * @param issue Elemento a procesar
	 * @param mesj Avisos
	 * @return cero
	*/
	public static void avisosFinTramitacion(RedmineIssue issue, 
		WarningData mesj) {

		/*Comprobamos si el issue se ha actualizado, si la actualización es por estado
		  y si el nuevo estado es fin*/
		if (issue.isUpdated() && issue.getStatusID() != null 
			&& "Exp. Fin tramites (Adju.)".equals(issue.getStatus().getName())) {
			mesj.add(issue, "La tramitación del contrato ha finalizado");
		}

	}

	/**
	 * Comprueba si finaliza un expediente y genera aviso
	 * 
	 * @param issue Elemento a procesar
	 * @param mesj Mensajes de error
	*/
	public static void avisosFechaFinProcessor(RedmineIssue issue, 
		WarningData mesj) {
		
		// Comprobamos fin de expediente pero por estado
		/*Comprobamos si el issue se ha actualizado, si la actualización es por estado
		  y si el nuevo estado es fin*/
		if (issue.isUpdated() && issue.getStatusID() != null 
			&& "Exp. Finalizado".equals(issue.getStatus().getName())) {
			mesj.add(issue, "El contrato ha finalizado");
		}
	}

	


	/**
	 * Procesa los identificadores del expediente en otros sistemas
	 * Comprobaciones:
	 *    - Núm. Sorolla en todos
	 *    - Núm. JC en abiertos
	 *
	 * @param issue Elemento a procesar
	 * @param mesj Mensajes de error
	 * @return Elementos actualizados (no actualiza así que cero siempre 
	 *	     	pero se deja preparado para cambios a futuro)
	*/
	public static int checkNumExpedientesProcessor(RedmineIssue issue, 
		WarningData mesj) {

		List<RedmineIssueCustomField> custom_fields = 
			issue.getRedmineIssueCustomFields();

		//logger.debug("Procesando identificadores de " + issue.getSubject());

		for (int i = 0; i < custom_fields.size(); i++) {
			try {
				String fieldName = custom_fields.get(i).getName();		

				// Núm. Expediente en Sorolla - Para todos
				// Nota: ponemos contains y no equals porque en windows
				// el caracter º del nombre del campo (Nº Exp. Sorolla) 
				// da problemas en el equals
				if (fieldName.contains(" Exp. Sorolla")) {					
					String value = (String)custom_fields.get(i).getValue();
					if (value == null || value.isEmpty()) 
						mesj.add(issue, "El campo " + fieldName + " está vacio y debe completarse");
				} 				
				
				// Núm. Expediente asignado por la junta
				// Nota: ponemos contains y no equals porque en windows
				// el caracter º del nombre del campo (Nº Exp. JC) 
				// da problemas en el equals
				if (fieldName.contains(" Exp. JC")) {					
					String value = (String)custom_fields.get(i).getValue();
					if (value == null || value.isEmpty()) 
						// comprobamos si el expediente está iniciado
						// en cuyo caso sí escribimos log de error
						if (!issue.isStatus("Exp. No iniciado")) 							
							mesj.add(issue, "El campo " + fieldName + " está vacio y debe completarse");
				} 				


			} catch (Exception e) { 
				logger.error("<checkNumExpedientesProcessor>" + e);
			}
		}

		return 0;
	}

	/**
	 * Procesa los importes de una petición
	 * Comprobaciones:
	 *  Existe importe total ($Importe total) e importes por año ($importe 20%)
	 *  y la suma de estos últimos es igual al importe total (tolerancia = 0.5)
	 *
	 * @param issue Elemento a procesar
	 * @param mesj Mensajes de error
	 * @return Elementos actualizados (no actualiza así que cero siempre 
	 *	     	pero se deja preparado para cambios a futuro)
	*/
	public static int checkImportesProcessor(RedmineIssue issue, 
		WarningData mesj) {

		List<RedmineIssueCustomField> custom_fields = 
			issue.getRedmineIssueCustomFields();

		double impTotal = -1;
		double sumImportes = 0.0;
		String warningStr = null;

		/*tolerancia en la comprobación entre importes*/
		double tolerancia = 0.5;
		
		// obtenemos datos
		//logger.debug("Procesando importes de " + issue.getSubject());
		for (int i = 0; i < custom_fields.size(); i++) {
			try {				
				if (custom_fields.get(i).getName().equals("$Importe total")) {
					String value = (String)custom_fields.get(i).getValue();
					if (value != null && !value.isEmpty()) 
						impTotal = Float.parseFloat(value);
				} else if (custom_fields.get(i).getName().contains("$Importe 20")) {
					String value = (String)custom_fields.get(i).getValue();
					if (value != null && !value.isEmpty())						
						sumImportes += Float.parseFloat(value);
				}
			} catch (Exception e) { 
				logger.error("<checkImportesProcessor>" + e);
			}
		}

		// procesamos resultados
		//logger.debug("ImporteTotal=" + impTotal + ";ImporteSum=" + sumImportes);
		if (impTotal == -1) 
			warningStr = new String("No hay importe total");
		else if (sumImportes == 0.0 && impTotal > 0 /*hay expedientes de importe=0*/ ) 
			warningStr = new String("No hay importes por año");
		else if (impTotal > 0/*hay expedientes de importe=0*/ 
				&& Math.abs(impTotal - sumImportes) > tolerancia) {

			/*se establece formato aleman porque tienen los mismos separadores que nosotros*/
			NumberFormat nf = NumberFormat.getNumberInstance(Locale.GERMAN);
			DecimalFormat formateador = (DecimalFormat)nf;			

			warningStr = 
				new String(
					"El importe total (" +  formateador.format(impTotal) + 
					") y la suma por año (" + formateador.format(sumImportes) + ") no coinciden por más de " 
					+ formateador.format(tolerancia));		
		}
		/*se guarda mensaje si ha habido algún error*/		
		if (warningStr != null)
			mesj.add(issue, warningStr);

		return 0;
	}

	/**
	 * Comprueba que la fecha de inicio de una petición es correcta
	 * 
	 * @param issue Elemento a procesar
	 * @param mesj Mensajes de error
	 * @return Elementos actualizados (no actualiza así que cero siempre 
	 *	     	pero se deja preparado para cambios a futuro)
	*/
	public static int checkFechaInicioImporteProcessor(RedmineIssue issue, 
		WarningData mesj) {

		// Obtenemos la fecha de inicio
		String fechaInicioStr = issue.getStartDate();
		if (fechaInicioStr != null && !fechaInicioStr.isEmpty()) {
			try {				
				Date fechaInicio = Helper.formatDateFromString(fechaInicioStr);								

				//Solo se revisarán importes cuando no sean contratos menores			
				//Comprobamos si el importe total no es cero					
				String importeTotal = issue.getRedmineCustomIssueFieldValue("$Importe total");
				if (!issue.isExpedienteMenor() && importeTotal != null 
					&& !importeTotal.isEmpty() 
					&& Double.parseDouble(importeTotal) > 0.0)  {

					String oldYear = null;
					// si la fecha es superior a la última de facturación del año 
					// y no es un suministro (por título del contrato)
					// (nota: los suministros se pueden gestionar a final de año sin problemas de 
					// facturación en ese año)
					if (fechaInicio.compareTo(Helper.getMaxFechaFacturacion(fechaInicio)) > 0
						&& !issue.getSubject().contains("uministro")) {
						//guardamos el año anterior
						oldYear = Helper.getYearOfDate(fechaInicio);
						//añadimos un año
						fechaInicio = Helper.addAYear(fechaInicio);
					}

					// obtenemos año que debe tener importe
					String year = Helper.getYearOfDate(fechaInicio);

					//se comprueba si existe anualidad en el año correcto
					String importeABuscar = "$Importe 20" + year;
					String importe = issue.getRedmineCustomIssueFieldValue(importeABuscar);					
					if (importe == null || importe.isEmpty()) {
						mesj.add(issue, 
							"De acuerdo con la fecha de inicio debería existir " + importeABuscar);
					}

					if (oldYear != null) {
						//se comprueba que no exista anualidad en el año en curso
						importeABuscar = "$Importe 20" + oldYear;
						importe = issue.getRedmineCustomIssueFieldValue(importeABuscar);
						if (importe != null && !importe.isEmpty()) {
							mesj.add(issue, 
								"De acuerdo con la fecha de inicio no debería existir " + importeABuscar);					
						}
					}
				}
			} catch (ParseException e) {
				logger.error("<checkFechaInicioProcessor>" + e);
				mesj.add(issue, "Fecha de inicio no compatible");	
			}
		} else {
			mesj.add(issue, "El expediente no tiene fecha de inicio definida");
		}


		return 0;
	}

	/**
	 * Comprueba que la fecha de fin de una petición es correcta
	 * 
	 * @param issue Elemento a procesar
	 * @param mesj Mensajes de error
	 * @return Elementos actualizados (no actualiza así que cero siempre 
	 *	     	pero se deja preparado para cambios a futuro)
	*/
	public static int checkFechaFinImporteProcessor(RedmineIssue issue, 
		WarningData mesj) {

		// Obtenemos la fecha fin
		String fechaFinStr = issue.getDueDate();
		if (fechaFinStr != null && !fechaFinStr.isEmpty()) {
			try {				
				Date fechaFin = Helper.formatDateFromString(fechaFinStr);

				//Comprobamos si no es un menor (al ser menor la fecha de fin no 
				// marca la anualidad), si no es un suministro (por título) 
				// y si el importe total no es cero				
				String importeTotal = issue.getRedmineCustomIssueFieldValue("$Importe total");
				if (!issue.isExpedienteMenor() //si es menor se paga a fecha de inicio
					&& !issue.getSubject().contains("uministro")
					&& importeTotal != null 
					&& !importeTotal.isEmpty() 
					&& Double.parseDouble(importeTotal) > 0.0)  {
				
					//se comprueba si existe anualidad de acuerdo a fecha
					String year = Helper.getYearOfDate(fechaFin);
					String importeABuscar = "$Importe 20" + year;
					String importe = issue.getRedmineCustomIssueFieldValue(importeABuscar);
					if (importe == null || importe.isEmpty())
						mesj.add(issue, 
							"De acuerdo con la fecha de fin debería existir " + importeABuscar);
				}
			} catch (ParseException e) {
				logger.error("<checkFechaFinProcessor>" + e);
				mesj.add(issue, "Fecha de fin no compatible");	
			}

		} else {
			mesj.add(issue, "El expediente no tiene fecha de fin definida");
		}


		return 0;
	}


	/**
	 * Procesamiento de los pagos de un expediente
	 * Se acumula la suma de los pagos de un expediente (campo $Importe 
	 * total en Exp: Pagos) en el campo $Importe facturado
	 * Además se añade un warning si estamos a punto de llegar al 85% de facturación
	 *
	 * @param issueExp: Expediente a procesar
	 * @param allIssues: Resto de peticiones recuperadas del servidor
	 * @param processExpMesj: Mensajes para el usuario
	 * @return 1 si el expediente es actualizado o 0 si no
	*/
	public static int updatePagosProcessor(RedmineIssue issueExp, 
		List<RedmineIssue> allIssues, WarningData mesj) {

		int issueUpdates = 0;
		double sumImportesPagos = 0.0;
		String warningStr = null;
		double limiteWarning = 0.85;

		//logger.debug("Procesando pagos de " + issueExp.getSubject());		
		for (int i = 0; i < allIssues.size(); i++) {			
			RedmineIssue issue = allIssues.get(i);						
			/*se recorren los elementos buscando hijos del expediente de tipo factura*/
			if (issue.isChildOf(issueExp) && issue.isFactura()) {
				//logger.debug("Procesando el pago " + issue.getSubject());		

				/*obtenemos importe de la factura*/
				String impStr = 
					issue.getRedmineCustomIssueFieldValue("$Importe total");
				if (impStr == null) {
					/*Error de configuración en REDMINE*/
					warningStr = 
						"ERROR: No hay definido campo \"$Importe total\" para los elementos de tipo Pago";
					logger.error("<updatePagosProcessor>" + warningStr);					
					mesj.add(issueExp, warningStr);					
					return 0;					
				} else if (!impStr.isEmpty()) {
					/*Sumatorio de importes de facturas*/
					sumImportesPagos += Double.parseDouble(impStr);					
				}
			}				
		}

		if (sumImportesPagos > 0) {			
			String impFactActualStr = 
				issueExp.getRedmineCustomIssueFieldValue(
					"$Importe facturado");			

			/*si el importe facturado actual no existe o es distinto al calculado*/
			if (impFactActualStr == null || impFactActualStr.isEmpty() 
				|| sumImportesPagos != Double.parseDouble(impFactActualStr)) {
				
				String sumImportesPagosStr = Double.toString(sumImportesPagos);

				/*se actualiza valor*/
				issueExp.changeImporteFacturado(sumImportesPagosStr);				
				issueUpdates++;

				/*mensaje para el usuario*/				
				warningStr = 
					"Se actualiza importe facturado de " + issueExp.getSubject() 
						+ " a " + sumImportesPagosStr;
						
				/*impresión del mensaje*/
				//logger.debug(warningStr);
				mesj.add(issueExp, warningStr);
			}
		}

		/*se compara el importe facturado respecto al total del expediente*/
		String impTotalExpStr = 
			issueExp.getRedmineCustomIssueFieldValue("$Importe total");	
		if (impTotalExpStr != null && !impTotalExpStr.isEmpty()) {

			Double impTotalExp = Double.parseDouble(impTotalExpStr);

			if (impTotalExp > 0 /*hay expedientes de importe=0*/ 
					&& sumImportesPagos == impTotalExp) {
				warningStr = "El importe facturado alcanza el máximo del expediente";					
			} else if (sumImportesPagos > impTotalExp) {
				warningStr = "El importe facturado supera el máximo del expediente";					
			} else if (impTotalExp > 0 /*hay expedientes de importe=0*/ 
					&& sumImportesPagos >= (impTotalExp*limiteWarning)) {
				warningStr = "El importe facturado supera el 85% del total";
			} 

			mesj.add(issueExp, warningStr);
		}

		return (issueUpdates > 0)?1:0;	
	}

	/**
	 * Procesamiento del estado de una petición en base al 
	 * workflow definido (workflow.json)
	 *
	 * @param issueExp Elemento a procesar
	 * @param workflow Workflow definido
	 * @param mesj Mensajes de aviso 
	 * @param server Servidor de redmine
	 * @return 1 si el expediente es actualizado o 0 si no
	*/
	public static int updateEstadosProcessor(RedmineIssue issueExp, 
		ExpedientesWorkflow workflow, WarningData mesj, 
		RedmineServer server) { 
		
		int issueUpdates = 0;
		String warningStr = null;
		RedmineIssueField newStatus = null;
		RedmineIssueField lastChangedStatus = null;

		//logger.debug("Procesando estados de " + issueExp.getSubject());			
		try {
			//se recupera el workflow para el tipo de petición
			ExpedientesWorkflowItem wfItem = 
				workflow.getWorkflowItemFor(
					issueExp.getTracker().getName());

			if (wfItem == null) {
				/*no workflow*/ 
				warningStr = "ERROR: No se ha encontrado workflow para expediente tipo " 
					+ issueExp.getTracker().getName();
				/*se avisa al usuario del error*/
				mesj.add(issueExp, warningStr);
				return 0;
			} 		
			//logger.debug("Workflow=" + wfItem);

			/*se recuperan del servidor los estados posibles */
			RedmineIssueStatuses statuses = server.getStatuses();
			if (statuses == null || statuses.getStatuses() == null 
				|| statuses.getStatuses().size() == 0) {
				/*no estados*/ 
				warningStr = 
					"ERROR: No se ha podido recuperar la lista de estados del servidor";
				/*se avisa al usuario del error*/
				mesj.add(issueExp, warningStr);
				return 0;
			}

			/* ITERACIONES SOBRE EL WORKFLOW*/ 
			/* Puede haber más de un cambio, es decir, podemos pasar del estado 1 al 3*/
			/* El número de transiciones viene definido en el fichero de configuración*/

			/* Nota: Para realizar varias iteraciones sobre el flujo de trabajo 
			se requiere tener permisos para saltar hacia adelante más de un estado*/

			int iterations = 0;								
			while (iterations < server.getMaxTransitions()) {
				// Se recupera la transición para el estado actual
				ExpedientesTransition transition = 
					wfItem.getTransitionFor(issueExp.getStatus().getName());
				if (transition == null) {
					/*no hay transición definida (puede ser estado final)*/ 
					/*warningStr = "ERROR: No se ha encontrado transición para expediente tipo \"" 
						+ issueExp.getTracker().getName() + "\" para estado actual \""
						+ issueExp.getStatus().getName() + "\"";*/
					
					/*logger.debug("No se ha encontrado transición para expediente tipo \"" 
						+ issueExp.getTracker().getName() + "\" para estado actual \""
						+ issueExp.getStatus().getName() + "\"");*/
					
					break; //out of while; no se aplican más transiciones

				} else {				
					/*Se evalua la transición por si procede, en cuyo caso
					  obtenemos el estado resultado de la transición*/	
					newStatus = getTransitionResult(issueExp, 
									warningStr, statuses, transition);
					//logger.debug("Estado nuevo:" + newStatus);
					if (newStatus != null) {
						/*aplicamos resultado obtenido*/
						issueExp.changeStatus(newStatus);						
						issueUpdates++;
						lastChangedStatus = newStatus;
						//logger.debug("Estado de la transición parcial: " + newStatus);
					} else {
						/* no hay resultado de la transición. debe haber llegado error*/						
						/*se avisa al usuario del error*/					
						mesj.add(issueExp, warningStr);
						break; //out of while; no se aplican más transiciones
					}
				}
				iterations++;
			}

			// Si se ha producido alguna transición se apunta la última efectuada como 
			// mensaje de usuario
			if (issueUpdates > 0 && lastChangedStatus != null ) {
				//logger.debug("Estado final de la transacción " + lastChangedStatus);
				warningStr = "CAMBIO: nuevo estado de la petición=" + lastChangedStatus.getName();
				mesj.add(issueExp, warningStr);
			} 
		} catch (RedmineServerException e) {
			// error at recover statuses from server
			logger.error("<updateEstadosProcessor>" +e);
			warningStr = e.getMessage();		
		} /*catch (Exception e) {
			// problemas con las fechas por ejemplo
			logger.error(e);
			warningStr = e.getMessage();		
		} */

		return (issueUpdates > 0)?1:0; 					
	}

	/**
	* Aplica una transición a un elemento
	*
	* @param issueExp: Elemento a cambiar
	* @param processExpMesjtr: String con error para usuario
	* @param statuses: Lista de estados recuperada del servidor
	* @param transition: Transición obtenida del workflow
	* @return Nuevo estado a aplicar o null si no ese aplica nuevo estado
	*/
	public static RedmineIssueField getTransitionResult(RedmineIssue issueExp, String warningStr,
		RedmineIssueStatuses statuses, 	ExpedientesTransition transition) {
		
		RedmineIssueField newStatus = null;		
		//logger.debug("Procesando transition para " + issueExp.getSubject());			

		try {				
			String value = null;		
			if (transition.getCampoComprobar().equals("Fecha fin")) {
				/*si la transición a comprobar es sobre el campo Fecha fin */
				/*se separa del resto porque no es un custom field y su tratamiento es disntinto*/
				value = issueExp.getDueDate();
			} else {
				RedmineIssueCustomField cf = 
					issueExp.getRedmineCustomIssueField(transition.getCampoComprobar());

				if (cf != null ) //campo no existente
					value = cf.getValue();
			}
						
			//logger.debug("Transición: " + transition);
			//logger.debug("Campo a comprobar: " + transition.getCampoComprobar());
			//logger.debug("Valor de campo a comprobar: " + value);

			/*si la transición a comprobar es de tipo existe (valor en el campo)*/
			if (transition.getComprobacion().equals("Existe")) {
				if (value != null && !value.isEmpty()) {
					newStatus = 
						statuses.getStatusByName(
							transition.getEstadoSiguiente());	
					if (newStatus == null)	
						warningStr = 
							"ERROR: No se ha encontrado estado " 
							+ transition.getEstadoSiguiente()
							+ " dentro de la lista para realizar la transición";									
				}
			/*si la transición es de tipo comprobación de un campo fecha con la fecha actual*/
			} else if (transition.getComprobacion().equals("Fecha")) {
				if (value != null && !value.isEmpty()) {
					Date now = new Date();
					//formateador de fechas en el formato redmine 
					Date valueToDate = Helper.formatDateFromString(value); 					

					if (value != null && now.after(valueToDate)) {						
						newStatus = 
							statuses.getStatusByName(transition.getEstadoSiguiente());
						if (newStatus == null)	
							warningStr = 
								"ERROR: No se ha encontrado estado " 
								+ transition.getEstadoSiguiente()
								+ " dentro de la lista para realizar la transición";										
					}
				}
			} else {
				warningStr = "ERROR: Tipo de comprobación " 
					+ transition.getComprobacion() 
					+ " especificada en el workflow no codificada en el programa";				
			}
		} catch (ParseException e) {
			//formateo del campo
			warningStr = "ERROR: Al parsear el campo " 
				+ transition.getCampoComprobar();
			logger.error("<getTransitionResult>" + warningStr + "(" + e + ")");
		
			return null;
		}

		return newStatus;
	}
	
}