package es.gob.catastro.sg.expediente;

import org.apache.log4j.Logger;

/**
 * Clase que almacena una transición entre estados si se cumple la comprobación
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */
public class ExpedientesTransition {
	private String estado_actual;
	private String campo_comprobar;
	private String comprobacion;
	private String estado_siguiente;

	static final Logger logger = Logger.getLogger(ExpedientesTransition.class);

	public ExpedientesTransition() {
		this.estado_actual = new String();
		this.campo_comprobar = new String();
		this.comprobacion = new String();
		this.estado_siguiente = new String();
	}

	public ExpedientesTransition(String estado_actual, 
		String campo, String comprobacion, String estado_siguiente) {
		
		this.estado_actual = estado_actual;
		this.campo_comprobar = campo;
		this.comprobacion = comprobacion;
		this.estado_siguiente = estado_siguiente;
	}

	public String getEstadoActual() {
		return this.estado_actual;
	}

	public void setEstadoActual(String estado_actual) {
		this.estado_actual = estado_actual;
	}

	public String getCampoComprobar() {
		return this.campo_comprobar;
	}

	public void setCampoComprobar(String campo_comprobar) {
		this.campo_comprobar = campo_comprobar;
	}

	public String getComprobacion() {
		return this.comprobacion;
	}

	public void setComprobacion(String comprobacion) {
		this.comprobacion = comprobacion;
	}

	public String getEstadoSiguiente() {
		return this.estado_siguiente;
	}

	public void setEstadoSiguiente(String estado_siguiente) {
		this.estado_siguiente = estado_siguiente;
	}

	public String toString() {
		String str = new String();

		str += "(eActual:\"" + getEstadoActual() + "\"";
		str += ", cComprobar:\"" + getCampoComprobar() + "\"";
		str += ", Comp:\"" + getComprobacion() + "\"";
		str += ", eSig:\"" + getEstadoSiguiente() + "\")";

		return str;
	}
}