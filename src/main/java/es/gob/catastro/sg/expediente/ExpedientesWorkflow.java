package es.gob.catastro.sg.expediente;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;


/**
 * Almacena el conjunto de flujos de trabajo definidos en el fichero 
 * para todos los tipos de expedientes 
 * 
 * @author Álvaro Tapias
 * @version 1.0
 * @see workflow.json
 */
public class ExpedientesWorkflow {
	private String descripcion;
	private List<ExpedientesWorkflowItem> workflow;

	static final Logger logger = 
		Logger.getLogger(ExpedientesWorkflow.class);

	public ExpedientesWorkflow() {
		this.descripcion = new String();
		this.workflow = new ArrayList<ExpedientesWorkflowItem>();
	}

	public ExpedientesWorkflow(String descripcion, List<ExpedientesWorkflowItem> wf) {
		this.descripcion = descripcion;
		this.workflow = new ArrayList<ExpedientesWorkflowItem>(wf);	
	}

	public String getDescripcion() {
		return this.descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = new String(descripcion);
	}

	public List<ExpedientesWorkflowItem> getWorkflow() {
		return this.workflow;
	}

	public void setWorkflow(List<ExpedientesWorkflowItem> workflow) {
		this.workflow = new ArrayList<ExpedientesWorkflowItem>(workflow);
	}

	public String toString() {
		String str = "Wf => Desc:" + getDescripcion() + 
			"; Items=" + getWorkflow().size() + ":\n";
		for (ExpedientesWorkflowItem item: getWorkflow()) {
			str += "{" + item.toString() + "}\n";
		}

		return str;
	}

	/**
	 * Recupera el workflow para un determinado tipo de expediente
	 *
	 * @param expType: Tipo de expediente
	 * @return Workflow del expediente si existe o null
	 * @see workflow.json
	*/
	public ExpedientesWorkflowItem getWorkflowItemFor(String expType) {
		for (ExpedientesWorkflowItem item: getWorkflow()) {
			if (item.getExpediente() != null 
					&& item.getExpediente().equals(expType)) 
				return item;
		}
		return null;
	}
}