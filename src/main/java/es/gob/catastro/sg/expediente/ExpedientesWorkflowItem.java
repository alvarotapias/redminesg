package es.gob.catastro.sg.expediente;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Almacena el workflow para un tipo de expediente concreto
 * 
 * @author Álvaro Tapias
 * @version 1.0
 * @see workflow.json
 */
public class ExpedientesWorkflowItem {
	private String expediente;
	private List<ExpedientesTransition> transiciones;

	static final Logger logger = Logger.getLogger(ExpedientesWorkflowItem.class);

	public ExpedientesWorkflowItem() {
		this.expediente = new String();
		this.transiciones = new ArrayList<ExpedientesTransition>();
	}

	public ExpedientesWorkflowItem(String expediente, 
		List<ExpedientesTransition> transiciones) {

		this.expediente = new String(expediente);
		this.transiciones = 
			new ArrayList<ExpedientesTransition>(transiciones);
	}	

	public String getExpediente() {
		return this.expediente;	
	}

	public void setExpediente(String expediente) {
		this.expediente = new String(expediente);
	}

	public List<ExpedientesTransition> getTransciones() {
		return this.transiciones;
	}

	public void setTransiciones(List<ExpedientesTransition> trans) {
		this.transiciones = new ArrayList<ExpedientesTransition>(trans);
	}

	public String toString() {
		String str = new String();

		str += "\tExp:" + getExpediente() + "\n";
		str += "\tTrans: [" + getTransciones() + "]";

		return str;
	}

	/**
	 * Recupera la transición para un determinado estado
	 *
	 * @param state Estado 
	 * @return Transición para ese estado
	 * @see workflow.json
	*/
	public ExpedientesTransition getTransitionFor(String state) {
		for (ExpedientesTransition transition: getTransciones()) {
			if (transition.getEstadoActual() != null 
					&& transition.getEstadoActual().equals(state)) 
				return transition;
		}
		return null;
	}

}