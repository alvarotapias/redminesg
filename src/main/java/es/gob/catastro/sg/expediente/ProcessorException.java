package es.gob.catastro.sg.expediente;

import es.gob.catastro.sg.SGException;

/**
 * Excepción de procesarmiento genérica
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */
public class ProcessorException extends SGException {
	private static final long serialVersionUID = 1L;

	public ProcessorException(String msg) {
	    super(msg);
	}
}