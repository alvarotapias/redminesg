package es.gob.catastro.sg.redmine;

import java.util.ArrayList;
import java.util.List;

/**
 * Petición de redmine
 * 
 * Almacena toda la información de una petición redmine
 *
 * @author Álvaro Tapias
 * @version 1.0
 */
public class RedmineIssue {
	private Integer 			id;
	private RedmineIssueField   project;
	private RedmineIssueField   tracker;
	private RedmineIssueField   status;
	private String  			status_id; //field for updating
	private RedmineIssueField   priority;
	private RedmineIssueField   author;
	private String  			subject;
	private RedmineIssueField   assigned_to;
	private RedmineIssueField   parent;
	private String  			description;
	private Integer 			done_ratio;
	private String  			created_on;
	private String  			start_date;
	private String  			due_date;
	private String  			updated_on;
	private List<RedmineIssueCustomField> custom_fields = 
		new ArrayList<RedmineIssueCustomField>();
	private String notes;
	private boolean updated;
	
	public RedmineIssue() { 
		this.id = 0;
		this.project = new RedmineIssueField();
		this.tracker = new RedmineIssueField();
		this.status  = new RedmineIssueField();
		this.priority = new RedmineIssueField();
		this.author = new RedmineIssueField();
		this.subject = null;
		this.assigned_to = new RedmineIssueField();
		this.parent = new RedmineIssueField();
		this.description = null;
		this.start_date = null;
		this.due_date = null;
		this.done_ratio = 0;
		this.created_on = null;
		this.updated_on = null;
		this.custom_fields = 
			new ArrayList<RedmineIssueCustomField>();
		this.notes = null;
		this.updated=false;
	}

	public RedmineIssue(Integer id,
						RedmineIssueField project,
						RedmineIssueField tracker,
						RedmineIssueField status,
						RedmineIssueField priority,
	                    RedmineIssueField author,
	                    String subject,
	                    RedmineIssueField assigned_to,
	                    RedmineIssueField parent,
	                    String description,
	                    String start_date,
	                    String due_date,
	                    Integer done_ratio,
	                    String created_on,
	                    String update_on,
	                    List<RedmineIssueCustomField> custom_fields) { 

		this.id = id;
		this.project      = new RedmineIssueField(project);
		this.tracker      = new RedmineIssueField(tracker);
		this.status       = new RedmineIssueField(status);
		this.priority     = new RedmineIssueField(priority);
		this.author       = new RedmineIssueField(author);
		this.subject      = subject;
		this.assigned_to   = new RedmineIssueField(assigned_to);
		this.parent       = new RedmineIssueField(parent);
		this.description  = description;
		this.start_date    = start_date;
		this.due_date    = due_date;
		this.done_ratio    = done_ratio;
		this.created_on    = created_on;
		this.updated_on    = update_on;
		this.custom_fields = custom_fields;	//ojo, referencia	
		this.notes = null;
		this.updated=false;
	}

	public RedmineIssue(Integer id,
						RedmineIssueField project,
						RedmineIssueField tracker,
						RedmineIssueField status,
						RedmineIssueField priority,
	                    RedmineIssueField author,
	                    String subject,
	                    RedmineIssueField assigned_to,
	                    RedmineIssueField parent,
	                    String description,
	                    String start_date,
	                    String due_date,
	                    Integer done_ratio,
	                    String created_on,
	                    String update_on,
	                    List<RedmineIssueCustomField> custom_fields,
	                    String notes) { 
		this.id = id;
		this.project       = new RedmineIssueField(project);
		this.tracker       = new RedmineIssueField(tracker);
		this.status        = new RedmineIssueField(status);
		this.priority      = new RedmineIssueField(priority);
		this.author        = new RedmineIssueField(author);
		this.subject       = subject;
		this.assigned_to    = new RedmineIssueField(assigned_to);
		this.parent        = new RedmineIssueField(parent);
		this.description   = description;
		this.start_date    = start_date;
		this.due_date      = due_date;
		this.done_ratio    = done_ratio;
		this.created_on    = created_on;
		this.updated_on    = update_on;
		this.custom_fields = custom_fields; //ojo, referencia
		this.notes         = notes;	
		this.updated=false;	
	}
	//
	public Integer getId() { 
		return this.id; 
	} 
	public void setId(Integer id) { 
		this.id = id; 
	}
	//
	public RedmineIssueField getProject() { 
		return this.project; 
	} 

	public void setProject(RedmineIssueField project) { 
		this.project = project; 
	}
	//
	public RedmineIssueField getTracker() { 
		return this.tracker; 
	} 

	public void setTracker(RedmineIssueField tracker) { 
		this.tracker = tracker; 
	}
	//
	public RedmineIssueField getStatus() { 
		return this.status; 
	} 
	//
	public void setStatus(RedmineIssueField status) { 
		this.status = status;
		this.status_id = Integer.toString(status.getId());
	}

	public void setStatus(String status, String numStatus) { 
		/*status field*/
		this.status.setName(status);
		this.status.setId(Integer.parseInt(numStatus));
		
		/*status_id field: Ojo, necesario para actualizar el elemento
		  a través del servicio*/
		this.status_id = numStatus;
	}

	public void setStatusID(String numstatus) { 
		this.status_id = numstatus;
	}

	public String getStatusID() { 
		return this.status_id;
	}
	//
	public RedmineIssueField getPriority() { 
		return this.priority; 
	} 
	
	public void setPriority(RedmineIssueField priority) { 
		this.priority = priority; 
	}
	//
	public RedmineIssueField getAuthor() { 
		return this.author; 
	} 
	public void setAuthor(RedmineIssueField author) { 
		this.author = author; 
	}

	//
	public String getSubject() { 
		return this.subject; 
	} 
	public void setSubject(String subject) { 
		this.subject = subject; 
	}
	//
	public RedmineIssueField getAssignedTo() { 
		return this.assigned_to; 
	} 

	public void setAssignedTo(RedmineIssueField assigned_to) { 
		this.assigned_to = assigned_to; 
	}
	//
	public RedmineIssueField getParent() { 
		return this.parent; 
	} 
	public void setParent(RedmineIssueField parent) { 
		this.parent = parent; 
	}
	//
	public String getDescription() { 
		return this.description; 
	} 
	public void setDescription(String description) { 
		this.description = description; 
	}
	//
	public String getStartDate() { 
		return this.start_date; 
	} 
	public void setStartDate(String startDate) { 
		this.start_date = startDate; 
	}
	//
	public String getDueDate() { 
		return this.due_date; 
	} 
	public void setDueDate(String dueDate) { 
		this.due_date = dueDate; 
	}
	//
	public Integer getDoneRatio() { 
		return this.done_ratio; 
	} 
	public void setDoneRatio(Integer doneRatio) { 
		this.done_ratio = doneRatio; 
	}
	//
	public String getCreatedOn() { 
		return this.created_on; 
	} 
	public void setCreatedOn(String createdOn) { 
		this.created_on = createdOn; 
	}
	//
	public String getUpdatedOn() { 
		return this.updated_on; 
	} 
	public void setUpdatedOn(String updatedOn) { 
		this.updated_on = updatedOn; 
	}	
	// 
	public List<RedmineIssueCustomField> getRedmineIssueCustomFields() { 
		return this.custom_fields; 
	} 
	// 
	public void setRedmineIssueCustomFields(List<RedmineIssueCustomField> custom_fields) { 
		this.custom_fields = custom_fields; 
	}
	// 
	public void setRedmineIssueCustomField(String name, String value) { 
		for (int i = 0; i < this.custom_fields.size(); i++) {
			if (this.custom_fields.get(i).getName().equals(name)){
		   		 this.custom_fields.get(i).setValue(value);
		   		 return;
		   	 }	    	 
		 }
	}
	public void setRedmineIssueCustomField(String name, List <String> value) { 
		for (int i = 0; i < this.custom_fields.size(); i++) {
		   	 if (this.custom_fields.get(i).getName().equals(name)) {
		   		this.custom_fields.get(i).clearField();
		   		for (int x =0; x < value.size(); x++) {
		   			this.custom_fields.get(i).setValue(value.get(x), x);
		   		}
		   	 }	    	 
		 }
	}
	// 
	public void addNote(String note) { 
		if (this.notes != null) {
			this.notes = this.notes + "\n\n" + note;
		} else {
		 	this.notes = note;
		}
	}

	public String getNotes() {
		return notes;
	}

	public void addCustomField(RedmineIssueCustomField custom_field) { 
		this.custom_fields.add(custom_field);
	}
	
	public RedmineIssueCustomField getRedmineCustomIssueField(String name) { 
		for (int i = 0; i < this.custom_fields.size(); i++) {
		   	 if (this.custom_fields.get(i).getName().equals(name) )		   	 
		   		 return this.custom_fields.get(i);
		   	 	    	 
		 }
		 return null;
	}

	public String getRedmineCustomIssueFieldValue(String name) { 
		for (int i = 0; i < this.custom_fields.size(); i++) {
		   	 if (this.custom_fields.get(i).getName().equals(name) )		   	 
		   		 return this.custom_fields.get(i).getValue();
		   	 	    	 
		 }
		 return null;
	}
	public void setRedmineCustomIssueField(String name, String value) { 
		for (int i = 0; i < this.custom_fields.size(); i++) {
		   	 if (this.custom_fields.get(i).getName().equals(name) )
		   	 	 this.custom_fields.get(i).setValue(value);		   	
		 }
	}	

	public String toString() {
		String s = new String();
		s += "\tid:" + getId() + "\n";
		s += "\tproject:" + getProject() + "\n";
		s += "\ttracker:" + getTracker() + "\n";
		s += "\tstatus:" + getStatus() + "\n";
		s += "\tstatus_id:" + status_id + "\n";
		s += "\tpriority:" + getPriority() + "\n";
		s += "\tauthor:" + getAuthor() + "\n";
		s += "\tsubject:" + getSubject() + "\n";
		s += "\tassigned_to:" + getAssignedTo() + "\n";
		s += "\tparent:" + getParent() + "\n";
		s += "\tdescription:" + getDescription() + "\n";
		s += "\tstart_date:" + getStartDate() + "\n";
		s += "\tdue_date:" + getDueDate() + "\n";
		s += "\tupdate_on:" + getUpdatedOn() + "\n";
		s += "\tcustom_fields: \n";
		for (RedmineIssueCustomField cf : getRedmineIssueCustomFields()) {
			s += "\t\t" + cf.toString() + "\n";
		}
		s += "\tnotes:" + getNotes();
		
		return s;
	}

	public boolean isChildOf(RedmineIssue parent) {
		if (getParent().getId() == parent.getId().intValue())
			return true;
		return false;
	}

	///////////////////////////////////////////////////////////////
	//							NOTA
	// Campos especializados en el procesamiento de expedientes  
	// y pagos de contratación 
	///////////////////////////////////////////////////////////////
	
	/**
	 * Cambia el estado del elemento
	 *
	 * @param newStatus Nuevo estado
	 * @return True si el estado se cambia o no
	*/
	public boolean changeStatus(RedmineIssueField newStatus) {
		if (newStatus != null) {
			//cambiamos el estado
			this.setStatus(newStatus);	
			//apuntamos que el elemento se ha modificado
			this.updated = true;
			return true;
		}

		return false;
	}

	/**
	 * Cambia el importe facturado de un expediente
	 *
	 * @param importe Importe a grabar
	 * @return True si el importe se cambia o no
	*/
	public boolean changeImporteFacturado(String importe){		
		this.setRedmineIssueCustomField("$Importe facturado", 
					importe);
		//apuntamos que el elemento se ha modificado
		this.updated = true;
		return true;
	}

	public boolean isUpdated() {
		return this.updated;
	}


	/**
	 * Comprueba si la petición es de tipo expediente
	 *	 
	 * @return true si el elemento es de tipo expediente
	 */
	public boolean isExpediente() {	
		if (this.getTracker().getName().startsWith("Exp: ")) 
			return true;
		return false;
	}


	/**
	 * Comprueba si la petición es de tipo expediente menor
	 *	 
	 * @return true si el elemento es de tipo expediente
	 */
	public boolean isExpedienteMenor() {	
		return isType("Exp: Menor");
	}


	/**
	 * Comprueba si la petición es de tipo Pago
	 *
	 * @return true si el elemento es de tipo Pago	
	 */
	public boolean isFactura() {		
		return isType("Pago");
	}

	/**
	 * Comprueba si la petición es de un determinado tipo
	 *
	 * @return true si el elemento es de tipo Pago	
	 */
	private boolean isType(String type) {
		if (this.getTracker().getName().equals(type)) 
			return true;
		return false;
	}


	/**
	 * Comprueba si una petición está en un estado determinado 
	 *
	 * @param status Estado a comprobar
	 * @return true si la petición está en estado = status
	 */
	public boolean isStatus(String status) {

		String value = this.getStatus().getName();		
		if (value != null && !value.isEmpty() && value.equals(status)) 
			return true;
		return false;
	}

	/**
	 * Comprueba si está activado el check de la petición 
	 * que indica que no se deben procesar los importes.  
	 *
	 * @return true si se deben procesar importes de la petición
	 */
	public boolean isRevisaImportes() {

		String value = this.getRedmineCustomIssueFieldValue("SRV_Revisar_importes");		
		if (value != null && !value.isEmpty() && value.equals("1")) 
			return true;
		return false;
	}

	/**
	 * Comprueba si el expediente está aún en tramitación
	 *
	 * @return true si se deben procesar importes de la petición
	 */
	public boolean isEnTramitacion() {
		if (!isStatus("Exp. Fin tramites (Adju.)") 
				&& !isStatus("Exp. Finalizado")
				&& !isStatus("Rechazada"))
			return true;
		return false;
	}

}
