package es.gob.catastro.sg.redmine;

import org.apache.log4j.Logger;

/**
 * Campo customizable de una petición redmine
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */


import java.util.ArrayList;
import java.util.List;

public class RedmineIssueCustomField {

	private String name;   
	private Integer id;    
	private Object value;  
	private Boolean multiple;  /*multiple values*/

	static final Logger logger = Logger.getLogger(RedmineResponse.class);

	
	public RedmineIssueCustomField() { 
		this.name = null;
		this.id = 0;
		this.multiple = false;
		this.value = new String();
	}

	public RedmineIssueCustomField(String name, 
									Integer id, 
									String value, 
									Boolean multiple) { 
		this.name = name;
		this.id = id;
		this.value = new String(value);
		this.multiple = multiple;
	}

	public RedmineIssueCustomField(String name, 
									Integer id, 
									List<String> value, 
									Boolean multiple) { 
		this.name = name;
		this.id = id;
		this.value = new ArrayList<String>(value); 
		this.multiple = multiple;
	}

	public RedmineIssueCustomField(String name, Integer id, 
									String value) { 
		this(name, id, value, false);
	}

	public RedmineIssueCustomField(String name, Integer id, 
									List<String> value) { 
		this(name, id, value, true);
	}

	public RedmineIssueCustomField(RedmineIssueCustomField copy) { 
		this.name = copy.name;
		this.id = copy.id;		
		this.multiple = copy.multiple;
		if (multiple)
			this.value = new ArrayList<String>((List<String>)copy.value); 
		else 
			this.value = new String((String)copy.value);
	}

	public String getName() { 
		return this.name; 
	} 

	public void setName(String name) { 
		this.name = name; 
	}

	public Integer getId() { 
		return this.id; 
	} 
	public void setId(Integer id) { 
		this.id = id; 
	}

	public String getValue() { 
		if (this.multiple ) /*antes !*/
			return ((List<String>)this.value).get(0);	
		
		return this.value.toString();
	}

	public void clearField() { 
		if (this.multiple) 
			this.value = new ArrayList<String>();
		else 
			this.value = new String();
	}

	public String getValue(Integer i) { 
		if ((!this.multiple) || i < 1) 
			return null;			

		if (((List<String>)this.value).size() <= i) 
			return null;

		return ((List<String>)this.value).get(i);
	}

	public void setValue(String value, Integer i) { 
		if ((!this.multiple) || i < 1) 
			this.value = value;			
		
		((List<String>) this.value).set(i, value);
	}

	public void setValue(String value) { 
		if (this.multiple == false) 
			this.value = value;
	}

	public void addValue(String value) { 
		((List<String>) this.value).add(value); 
	}

	public String toString() {
		String s = new String();

		s += "{Nombre: " + getName() + ",Id: " + getId() + ",Valor: ";		
		if (this.multiple) {
			ArrayList<String> aList = new ArrayList((List<String>) this.value);
			for (int i=0; i<aList.size(); i++) {
				s += aList.get(i).toString();
			}
		} else 
			s += this.value;

		s += "}";
		return s;
	}	
}
