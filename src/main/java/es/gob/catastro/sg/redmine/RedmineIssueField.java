package es.gob.catastro.sg.redmine;


/**
 * Campo básico (por defecto) en redmine
 * Tiene dos propiedades: id y nombre
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */
public class RedmineIssueField {
	private String name;  
	private int id;       
	
	public RedmineIssueField() { 
		this.name = null;
		this.id = 0;
	}
	public RedmineIssueField(String name, int id) { 
		this.name = name;
		this.id = id;
	}
	public RedmineIssueField(RedmineIssueField copy) { 
		this.name = copy.name;
		this.id = copy.id;
	}
	public String getName() { 
		return this.name; 
	} 
	public void setName(String name) { 
		this.name = name; 
	}
	public int getId() { 
		return this.id; 
	} 
	public void setId(int id) { 
		this.id = id; 
	}
	public String toString() {
		return "{Name: \"" + this.name + "\", Id: "+ this.id + "}";
	}
}
