package es.gob.catastro.sg.redmine;

import java.util.ArrayList;
import java.util.List;

/**
 * Estados de una petición 
 *
 * @author Álvaro Tapias
 * @version 1.0
 */
public class RedmineIssueStatuses {
	private List<RedmineIssueField> issue_statuses;

	public RedmineIssueStatuses() {
		this.issue_statuses = new ArrayList<RedmineIssueField>();
	}

	public RedmineIssueStatuses(List<RedmineIssueField> st) { 
		this.issue_statuses = new ArrayList<RedmineIssueField>(st);
	}	

	public List<RedmineIssueField> getStatuses() {
		return this.issue_statuses;
	}

	public void setStatuses(List<RedmineIssueField> list) {
		this.issue_statuses = list;
	}

	public RedmineIssueField getStatusByName(String name) {
		if (name == null || name.isEmpty())
			return null;

		for (RedmineIssueField status: getStatuses()) {
			if (name.equals(status.getName())) 
				return status;
		}

		return null;
	}

	public String toString() {
		String str = new String();

		str +=  "[";
		for (RedmineIssueField status: getStatuses()) 
			str += status + ",";
		str +=  "]";

		return str;
	}
}