package es.gob.catastro.sg.redmine;


/**
 * Parámetros de la petición al servidor Redmine
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */
public class RedmineRequestParameters {
	int offset;
	int limit;
	String query;     
	String sort;      

	public RedmineRequestParameters() {
		offset = 0;
		limit = 100;
		sort = null;
		query = null;
	}

	/**
	 * Constructor
	 * 
	 * @param offset Primer elemento a recuperar en la petición
	 * @param limit Máximo número de elementos a recuperar	 
	 * @param query Campos específicos a incluir en la query
	 *        (Ej: updated_on=%3E%3D2017-04-01T08:12:32Z) 
	 * 	      Los campos deben estar separados por "&" 
	 * @param sort Campos de ordenación (Ej: sort=updated_on:desc)
	 * @see Sintáxis de las peticiones en http://www.redmine.org/projects/redmine/wiki/Rest_Issues
	 */
	public RedmineRequestParameters(int offset, 
									int limit, 
									String query, 
									String sort) {
		this.offset = offset;
		this.limit = limit;
		this.query = query;
		this.sort = sort;
	}


	public void setOffset(int offset) {
		this.offset = offset;
	}

	public int getOffset() {
		return offset;
	}

	public void setLimit(int limit) {
		this.limit = limit;
	}

	public int getLimit() {
		return limit;
	}

	public void setSortString(String sort) {
		this.sort = sort;
	}

	public String getSortString() {
		return sort;
	}

	public void setQueryString(String query) {
		this.query = query;
	}

	public String getQueryString() {
		return query;
	}

}