package es.gob.catastro.sg.redmine;

import java.util.ArrayList;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Respuesta del servidor a una petición 
 * Contiene la lista de elementos recuperada
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */

public class RedmineResponse {	
	private List<RedmineIssue> issues;
	private Integer offset;
	private Integer limit;

	//Log
	static final Logger logger = Logger.getLogger(RedmineResponse.class);

	public RedmineResponse() { 
		this.issues = new ArrayList<RedmineIssue>();
	}

	public RedmineResponse(List<RedmineIssue> peticion) { 
		this.issues = peticion;
	}
	
	public List<RedmineIssue> getIssues() { 
		return issues; 
	}
	public RedmineIssue getIssues(Integer id) {
		for(RedmineIssue issue: this.getIssues()) {
			if (issue.getId().equals(id))
				return issue;
		}
	
		return null; 
	} 
	
	public void setIssues(List<RedmineIssue> issues) { 
		this.issues = issues; 
	}
	
	public void addIssue(RedmineIssue issue) { 
		this.issues.add(issue);
	}

	public Integer getIssuesCount() {
		return this.issues.size();
	}
	
	public void addIssuesList(RedmineResponse issues) { 
		for(RedmineIssue i : issues.getIssues()) 			
			this.issues.add(i);		
	}

	public Integer getOffset() {
		return offset;
	}

	public void setOffset(Integer offset) {
		this.offset = offset;
	}

	public Integer getLimit() {
		return limit;
	}
	
	public void setLimit(Integer limit) {
		this.limit = limit;
	}

	public String toString() {
		String s = new String();
		
		s += "#issues=" + getIssuesCount() + "\n";
		s += "(offset=" + offset + "; limit=" + limit + ")" + "\n";
		for (RedmineIssue i : getIssues()) 
			s += "[" + i.toString() + "]\n---\n";		

		return s;
	}
}