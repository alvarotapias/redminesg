package es.gob.catastro.sg.redmine;

import org.apache.log4j.Logger;
import java.util.Properties;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;

import es.gob.catastro.sg.Helper;

/**
 * SERVIDOR Redmine  
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */
public class RedmineServer {
	public String server_url;
	public String key;
	public String project;
	public String timeout;
	public int maxTransitions;
	protected RedmineIssueStatuses statuses;

	//Log
	static final Logger logger = Logger.getLogger(RedmineServer.class);

	/**
	 * Constructor 
	 *
	 * @author Alvaro Tapias
	 * @param Propiedades de conexión al servidor (url, clave acceso, timeout, proyecto, ..)
	 */
	public RedmineServer(Properties prop) {
		server_url = prop.getProperty("redmine.server.url");
		key = prop.getProperty("redmine.server.key");
		project = prop.getProperty("redmine.server.project");
		timeout = prop.getProperty("redmine.server.timeout");
		statuses = null;
		String maxTransitionsStr = prop.getProperty("redmine.server.maxtransitions");
		maxTransitions = Integer.parseInt(maxTransitionsStr);
	}
	
	public int getMaxTransitions() {
		return maxTransitions;
	}

	public String toString() {
		return "Url: " + server_url + ", Key: " + key + ", Project: " + project
				+ ", Timeout: " + timeout;
	}

	/**
	 * Retorna la URL para una petición de tipo GET
	 *
	 * @author Alvaro Tapias
	 * @param params Parámetros necesarios para construir la petición (url servidor, proyecto)
	 * @return String URL para petición GET
	 * @see http://www.redmine.org/projects/redmine/wiki/Rest_Issues
	 */
	public String getUrlToGet(RedmineRequestParameters params) {
		String url = new String();

		url += server_url +"/projects/" + project;
		url += "/issues.json?key=" + key;
		url += "&status_id=open";	

		if (params != null && params.getOffset() > 0) 
			url += "&offset=" + params.getOffset();
		else 
			url += "&offset=0";

		if (params != null && params.getLimit() > 0) 
			url += "&limit=" + params.getLimit();	

		if (params != null && params.getQueryString() != null) 
			url += "&" + params.getQueryString();	

		if (params != null && params.getSortString() != null) 
			url += "&" + params.getSortString();	


		return url;
	}
	
	

	/**
     * Retorna todas las peticiones que cumplen la query del servidor
     * @author Alvaro Tapias
     * @param params Parámetros de la consulta (offset, limit, ...)
     * @return Redmine Respuesta con todas las peticiones seleccionadas
     * @throw Errors de conexión con el servidor
	 */
	public RedmineResponse getIssues(RedmineRequestParameters params) 
		throws RedmineServerException {	
		
		RedmineResponse response = null;
		HttpURLConnection con = null;
		String errorStr = null;

		/*URL de acceso a redmine*/
		String requestURL = getUrlToGet(params);
	    
	    try {	    	
	        URL url = new URL(requestURL);
	        con = (HttpURLConnection)url.openConnection();
	        con.setRequestMethod("GET");
	        con.setRequestProperty("Content-length", "0");
	        con.setUseCaches(false);
	        con.setAllowUserInteraction(false);
	        con.setConnectTimeout(Integer.parseInt(this.timeout));
	        con.setReadTimeout(Integer.parseInt(this.timeout));
	        con.connect();
	        logger.debug("<getIssues> GET a: " + requestURL);

	        int status = con.getResponseCode();
	        logger.debug("<getIssues> Respuesta servidor=" + status);
	        switch (status) {
	        	case 200:/*OK*/
	            case 201:/*CREATE*/  	            	     
	            	BufferedReader br = 
	            		new BufferedReader(
	            			new InputStreamReader(con.getInputStream()
	            				/*, "UTF-8"*/));
	            	
	                StringBuilder sb = new StringBuilder();
	                String line;
	                while ((line = br.readLine()) != null) {
	                    sb.append(line+"\n");
	                }
	                //logger.debug(sb);

	                Gson gson = new Gson();
	                response = 
	                	gson.fromJson(sb.toString(), RedmineResponse.class);
	                logger.debug("<getIssues> Response: " + response);	                
	                break;
	            default:	            	
	            	errorStr = "Error en servicio: código recibido de servidor=" + status;
	            	logger.error("<getIssues>" + errorStr);
	            	break;
	        }
	    } catch (MalformedURLException e) {	  
	    	errorStr = "MalformedURLException: "+ e.getMessage();
	    	logger.error("<getIssues>" + errorStr);	    	
	    } catch (IOException e) {
	    	errorStr = "IOException: "+ e.getMessage();
	    	logger.error("<getIssues>" + errorStr);	
	    	
	    } finally {
	       if (con != null) {
	          try {
	              con.disconnect();
	          } catch (Exception e) {
	          		errorStr = "Exception: "+ e.getMessage();
	    			logger.error("<getIssues>" + errorStr);	        		
	          }
	       }
	    }	

	    if (errorStr != null)
			throw new RedmineServerException(errorStr);

	    return response;	
	}

	/**
	 * Retorna la URL para hacer PUT (actualización) de un elemento
	 *
	 * @author Alvaro Tapias
	 * @param Elemento sobre el que se quiere realizar la actualización
	 * @return String URL de la petición
	 * @see http://www.redmine.org/projects/redmine/wiki/Rest_Issues
	 */
	public String getUrlToPut(RedmineIssue issueToPut) {
		String url = new String();

		url += server_url + "/issues/" + issueToPut.getId() +
				".json?key=" + key;		
		
		return url;
	}
	

	/**
	 * Retorna un string con la representación en JSON del elemento 
	 * a actualizar en el servidor
	 *
	 * @author Alvaro Tapias
	 * @param Elemento sobre el que se quiere realizar la actualización
	 * @return String Elemento en formato JSON 
	 * @see http://www.redmine.org/projects/redmine/wiki/Rest_Issues
	 */
	public String getIssueInfoToPut(RedmineIssue issueToPut) {
		
		Gson gson = new Gson();
		String jsonStr = "{\"issue\":" 
				+ gson.toJson(issueToPut)
				+ "}";
		return jsonStr;
	}

	/**
     * Actualizar un elemento en el servidor mediante un PUT
     * @author Alvaro Tapias
     * @param issueToUpdate Elemento a actualizar    
     * @throw Error al actualizar
	 */
	public void updateIssue(RedmineIssue issueToUpdate) throws RedmineServerException {
		String errorStr = null;
		HttpURLConnection c = null;
		OutputStreamWriter out = null;
		String urlToPut = getUrlToPut(issueToUpdate);
		String json = null;
		
		//logger.debug("URL:" + urlToPut);		
		
	    try {
	        URL u = new URL(urlToPut);
	        c = (HttpURLConnection)u.openConnection();
	        c.setRequestMethod("PUT");
	        c.setRequestProperty("Content-length", "0");
            c.setRequestProperty("Content-Type", "application/json");
            c.setRequestProperty("Accept", "application/json; charset=utf=8");                        
	        c.setDoOutput(true);
	        c.setUseCaches(false);
	        c.setAllowUserInteraction(false);
	        c.setConnectTimeout(Integer.parseInt(this.timeout));
	        c.setReadTimeout(Integer.parseInt(this.timeout));
	        c.connect();
	        logger.debug("<updateIssue> PUT a: " + urlToPut);	        
	        
	        out = 
	        	new OutputStreamWriter(c.getOutputStream()/*, "UTF-8"*/);

	        /*obtenemos el JSON*/	        
	        json = getIssueInfoToPut(issueToUpdate);

	        out.write(json);
	        out.flush();
	        //out.close();

	        c.getInputStream();	        	        
	        int status = c.getResponseCode();
	        logger.debug("<updateIssue> Respuesta del servidor=" + status);
	        switch (status) {
	            case 200: //OK
	            	logger.debug("<updateIssue> Actualizada la peticion " 
	            		+ issueToUpdate.getId() + ": " + status);
	            	break;
	            case 201: //OK
	            	logger.debug("<updateIssue> Actualizada la peticion " 
	            		+ issueToUpdate.getId() + ": " + status);
	            	break;
	            default:
	            	errorStr =  "Retorno no esperado al actualizar la peticion " 
	            		+ issueToUpdate.getId() + ": status=" + status 
	            		+ "; message=" + c.getResponseMessage();	            	           		            	
	            	break;
	        }	       
	    } catch (MalformedURLException e) {	
	    	errorStr = "MalformedURLException: "+ e.getMessage() + "\n[JSON:" + json + "]";	    	
	    } catch (IOException e) {
	    	errorStr = "IOException: "+ e.getMessage() + "\n[JSON:" + json + "]";
	    } finally {	    
	    	/*Cierre de conexiones*/
			if (out != null) {
	          try {
	          	out.close();
	          } catch (Exception e) {
	          		errorStr = "Exception: "+ e.getMessage();          		
	          }
	       	}

	        if (c != null) {
	          try {
	              c.disconnect();
	          } catch (Exception e) {
	          		errorStr = "Exception: "+ e.getMessage();          		
	          }
	        }
	    }		
	    
	    if (errorStr != null) {
	       	logger.error("<updateIssue>" + errorStr);	 
	       	throw new RedmineServerException(errorStr);
	    }
	}

	public String getUrlToGetStatuses() {
		String url = new String();

		url += server_url + "/issue_statuses.json?key=" + key;	;		
		
		return url; 
	}

	/**
     * Recupera la lista de estados para los elementos del servidor
     * @author Alvaro Tapias
     * @return Estados 
     * @throw Error de conexión 
	 */
	public RedmineIssueStatuses getStatuses() throws RedmineServerException {
		if (statuses == null) {
			HttpURLConnection con = null;
			String errorStr = null;
			
			/*Get request to redmine*/
			String requestURL = getUrlToGetStatuses();

			try {	    	
				URL url = new URL(requestURL);
				con = (HttpURLConnection)url.openConnection();
				con.setRequestMethod("GET");
				con.setRequestProperty("Content-length", "0");
				con.setUseCaches(false);
				con.setAllowUserInteraction(false);
				con.setConnectTimeout(Integer.parseInt(this.timeout));
				con.setReadTimeout(Integer.parseInt(this.timeout));
				con.connect();
				logger.debug("<getStatuses> GET a: " + requestURL);

				int status = con.getResponseCode();
				logger.debug("<getStatuses> Respuesta servidor=" + status);
				switch (status) {
					case 200:/*OK*/
					case 201:/*OK*/             		            	
					BufferedReader br = 
					new BufferedReader(
						new InputStreamReader(con.getInputStream()));
					StringBuilder sb = new StringBuilder();
					String line;
					while ((line = br.readLine()) != null) {
						sb.append(line+"\n");
					}
					
					Gson gson = new Gson();
					statuses = 
						gson.fromJson(sb.toString(), RedmineIssueStatuses.class);
					logger.debug("<getStatuses> Statuses: " + statuses);
					break;
				default:	            						
					errorStr =  "Retorno no esperado al consultar los estados en el servidor " 
	            		+ ": status=" + status 
	            		+ "; message=" + con.getResponseMessage();	      
					break;
				}
			} catch (MalformedURLException e) {	  
				errorStr = "MalformedURLException: " + e.getMessage();  	
				logger.error("<getStatuses>" + errorStr);				
			} catch (IOException e) {
				errorStr = "IOException: " + e.getMessage();  	
				logger.error("<getStatuses>" + errorStr);
			} finally {				
				if (con != null) {
					try {
						con.disconnect();
					} catch (Exception e) {
						errorStr = "Exception: " + e.getMessage();  	
						logger.error("<getStatuses>" + errorStr);
					}
				}
			}	

			if (errorStr != null) {
				logger.error("<getStatuses>" + errorStr);
				throw new RedmineServerException(errorStr);	
			}
		}
		return statuses;
	}

	/**
     * Retorna la URL de acceso al detalle de una petición
     * @author Alvaro Tapias
     * @param issue Elemento al que queremos acceder en el servidor
     * @return URL
	 */
	public String getIssueUrl(RedmineIssue issue) {
		return server_url + "/issues/" + issue.getId();
	}

}