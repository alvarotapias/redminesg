package es.gob.catastro.sg.redmine;

/**
 * Excepción genérica Redmine
 * 
 * @author Álvaro Tapias
 * @version 1.0
 */
public class RedmineServerException extends Exception {
	
	private static final long serialVersionUID = 1L;

	public RedmineServerException(String msg) {
	    super(msg);
	}
}