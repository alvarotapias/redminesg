package es.gob.catastro.sg.warning;

import java.util.HashMap;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Map;
import java.util.Date;
import org.apache.log4j.Logger;

import es.gob.catastro.sg.redmine.RedmineResponse;
import es.gob.catastro.sg.redmine.RedmineIssue;
import es.gob.catastro.sg.redmine.RedmineServer;
import es.gob.catastro.sg.Helper;


/**
 * Almacena mensajes de error o información a notificar al usuario
 *
 * @author Alvaro Tapias
 * @version 1.0
*/
public class WarningData extends HashMap<RedmineIssue, ArrayList<String>> {
	
	static final Logger logger = Logger.getLogger(WarningData.class);

	//datos del destino del mensaje
	protected String mailTo;
	protected String mailSubject;

	/**
	 * Constructor
	*/
	public WarningData(String to, String subject) {
		mailTo = to;
		mailSubject = subject;
	}

	public String getMailTo() {
		return mailTo;
	}

	public String getMailSubject() {
		return mailSubject;
	}

	public int getSize() {
		return this.size();
	}


	/**
	 * Añade un mensaje a lo existente
	 * @author Alvaro Tapias
	 * @param issue Elemento o petición redmine
	 * @param warning Texto a añadir
	*/
	public void add(RedmineIssue issue, String warning) {

		//logger.debug("Añadiendo warning [" + warning + "] para " + issue.getSubject());
		if (warning != null && !warning.isEmpty()) {
			ArrayList<String> warnArray = this.get(issue);
			if (warnArray == null) {
				warnArray = new ArrayList<String>();
				warnArray.add(warning);
				/*call to super.put()*/
				put(issue, warnArray);
			} else {
				warnArray.add(warning);
			}
		}
	}

	/**
	 * Imprime a String
	 * @author Alvaro Tapias
	 * @return String WarningData String con la información del objeto
	*/
	public String toString() {
		String s = new String();

		s = "Mensaje para:" + getMailTo() + "\n";
		
		if (this.size() == 0) {
			s += "No se han registrado avisos";
			return s;
		}

		for (Map.Entry<RedmineIssue, ArrayList<String>> e: this.entrySet()) {
			s += "\tPetición: (" + e.getKey().getTracker().getName() + ")" 
				+ e.getKey().getSubject() 
				+ " - Asignado a " + e.getKey().getAssignedTo().getName()  
				+ "\n";
			s += "\t{\n"; 
			ArrayList<String> warnArray = e.getValue();
			for (int x=0; x<warnArray.size(); x++)
				s += "\t\t" + warnArray.get(x) + "\n";			
			s += "\t}\n";
		}

		return s;
	}

	/**
	 * Imprime a un String con código html en su interior
	 * Usado para mandar mail
	 * @author Alvaro Tapias
	 * @param server Servidor Redmine 
	 * @return String WarningData String con el óbjeto en código HTML
	*/
	public String toHtml(RedmineServer server) {
		String s = new String();		

		s += "<font size=\"2\" face=\"verdana\" color=\"black\">";
		s += "<h3>PROCESAMIENTO AUTOMÁTICO REALIZADO EN REDMINE</h3>";
		s += "<div>Este correo ha sido enviado automáticamente y no debe ser contestado.<br>";
		s += "La comprobación y los cambios han sido realizados el " +
				Helper.formatDateToString(new Date()) +".</div>";

		//se comprueba si hay elementos a listar. 
		//en caso de que no se incluye mensaje y se retorna		
		if (this.size() <= 0) {
			s += "<p><b>No hay mensajes de error a mostrar (todos los expedientes son correctos).</b></p></font>";
			return s;
		}

		//hay elementos a procesar
		s += "<u><h3>Expedientes con información a revisar</h3></u>";		
		for (Map.Entry<RedmineIssue, ArrayList<String>> e: this.entrySet()) {
			s += 
				"<div><b><a href=\""+ server.getIssueUrl(e.getKey()) + "\">" 				
				+ "(#" + e.getKey().getId() + ") " 
				+ e.getKey().getTracker().getName() + ": " 
				+ e.getKey().getSubject()
				+ "</a></b>";
			if (e.getKey().getAssignedTo().getName() != null)
				s += " (Asignado a <b>@" + e.getKey().getAssignedTo().getName() + "</b>)";
			else 
				s += " (<b>No asignado</b>)" ;

			s += "</div>";

			s += "<div>Información del expediente: <ul>"; 
			ArrayList<String> warnArray = e.getValue();
			for (int x=0; x<warnArray.size(); x++)
				s += "<li>" + warnArray.get(x) + ".</li>";			
			s += "</ul></div>";
		}		
		s+="</font>";

		return s;
	}
}