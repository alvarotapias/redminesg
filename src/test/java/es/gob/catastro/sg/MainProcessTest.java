package es.gob.catastro.redminesg;

import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;
import java.util.Properties;
import org.apache.log4j.Logger;

import es.gob.catastro.sg.MainProcess;
import es.gob.catastro.sg.Helper;
import es.gob.catastro.sg.SGException;
import es.gob.catastro.sg.expediente.*;
import es.gob.catastro.sg.redmine.*;
import es.gob.catastro.sg.warning.*;


/**
 * Unit test for simple App.
 */
public class MainProcessTest 
    extends TestCase 
{

    static final Logger logger =  Logger.getLogger(MainProcessTest.class);

    /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public MainProcessTest( String testName ) {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite( MainProcessTest.class );
    }

    /**
     * Rigourous Test :-)
     */
    public void testApp() {
        logger.debug("Inicializando test ExpedientesProcessorTest");
        
        try {
            //Lectura del fichero de propiedades
            Properties props = Helper.readProperties(MainProcess.configFile);
            logger.debug(props);  
        
            // creación de issue
            RedmineIssue issueExp = new RedmineIssue();
        
            //mensajes de procesamiento
            String revisionTo = props.getProperty("revision.mail.to");
            assertTrue(revisionTo != null);

            String subjectTo = props.getProperty("revision.mail.subject");
            assertTrue(subjectTo != null);            
            
            WarningData expProcessMensajes = new WarningData(
                revisionTo, subjectTo);

            // workflow
            ExpedientesWorkflow workflow = Helper.readWorkflow();
            assertTrue(workflow != null && workflow.getWorkflow().size() > 0);         

        } catch (SGException e) {
            logger.error("TEST ERROR: " + e);                       
        }

        assertTrue( true );
    }
}
