package es.gob.catastro.sg.expediente;

import org.apache.log4j.Logger;
import junit.framework.Test;
import junit.framework.TestCase;
import junit.framework.TestSuite;

import es.gob.catastro.sg.expediente.*;
import es.gob.catastro.sg.redmine.*;
import es.gob.catastro.sg.warning.*;
import es.gob.catastro.sg.Helper;
import es.gob.catastro.sg.SGException;


/**
 * Unit test for simple App.
 */
public class ExpedientesProcessorTest 
    extends TestCase {	

   /**
     * Create the test case
     *
     * @param testName name of the test case
     */
    public ExpedientesProcessorTest( String testName ) {
        super( testName );
    }

    /**
     * @return the suite of tests being tested
     */
    public static Test suite() {
        return new TestSuite(ExpedientesProcessorTest.class);
    }


    /**
     * Test changes in workflow
     */
    public void testEstadosProcessor() { 	    	
        assertTrue(true);
    }

}